package com.franklinht.bill.utils;

import junit.framework.TestCase;

import org.junit.Assert;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by franklin on 03/09/15.
 */
public class UtilsTest extends TestCase {

//    @Test
    public void testConvertStringToCalendar(){
        Assert.assertEquals(22, Utils.convertStringToCalendar("2015-11-22", Locale.ENGLISH).get(Calendar.DAY_OF_MONTH));
        Assert.assertNotEquals(11, Utils.convertStringToCalendar("2015-11-22", Locale.ENGLISH).get(Calendar.DAY_OF_MONTH));
        Assert.assertNotEquals(2015, Utils.convertStringToCalendar("2015-11-22", Locale.ENGLISH).get(Calendar.DAY_OF_MONTH));

    }

//    @Test
    public void testFormatNumber(){
        //TEST esta fazendo conversao correta
        assertEquals("R$1,23", Utils.formatNumber("123"));
        assertEquals("R$1,00", Utils.formatNumber("100"));
        assertEquals("R$1,20", Utils.formatNumber("120"));
        assertEquals("R$10,23", Utils.formatNumber("1023"));
        assertEquals("R$100,00", Utils.formatNumber("10000"));
        assertEquals("R$1.000,00", Utils.formatNumber("100000"));

        //TEST esta fazendo conversao incorretamente
        Assert.assertNotEquals("R$0,01", Utils.formatNumber("100"));
        Assert.assertNotEquals("R$1,2", Utils.formatNumber("120"));
        Assert.assertNotEquals("R$1.023,00", Utils.formatNumber("1023"));
        Assert.assertNotEquals("R$10.000,00", Utils.formatNumber("10000"));
    }

    public void testGetMonthName3FromInt(){
        //TEST esta fazendo calculo correto
        Assert.assertNotEquals("setembro", Utils.getMonthName3FromInt(8));
        //TEST Nao esta cropando para 3 digitos
        Assert.assertEquals("SET", Utils.getMonthName3FromInt(8));
        //TEST Esta fazendo calculo correto
        Assert.assertNotEquals("SET", Utils.getMonthName3FromInt(5));
        //TEST lingua estrangeira
        Assert.assertNotEquals("may", Utils.getMonthName3FromInt(5));
        //TEST erro na funcao
        Assert.assertNotNull(Utils.getMonthName3FromInt(8));
    }
}

package com.franklinht.bill;

import android.test.ActivityInstrumentationTestCase2;

import com.franklinht.bill.ui.MainActivity_;

import org.junit.Before;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by franklin on 04/09/15.
 */
public class FaturaFragmentTest extends ActivityInstrumentationTestCase2<MainActivity_> {
    //public class FaturaFragmentTest {
    private MainActivity_ main;

    public FaturaFragmentTest() {
        super(MainActivity_.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        main = getActivity();
    }

    //Teste em elementos graficos
    public void testeFragments_MainActivity() {
//        onView(withId(R.id.viewpager_monthcontent))
//                .perform(click())
//                .check(matches(isDisplayed()));
//        onView(withId(R.id.button_gerarboleto))
//                .perform(click());
//        onView(withId(R.id.listview_fatura)).perform(scrollTo(), click());

        onView(withId(R.id.viewpager_monthcontent)).perform(swipeLeft());
//        onView(allOf(withId(R.id.button_gerarboleto))).perform(click());
    }

    /**
     * testes para delay
     */
    public void testPassScreens() {
        onView(withId(R.id.viewpager_monthcontent)).perform(swipeLeft());
        onView(withId(R.id.viewpager_monthcontent)).perform(swipeLeft());
        onView(withId(R.id.viewpager_monthcontent)).perform(swipeLeft());
        onView(withId(R.id.viewpager_monthcontent)).perform(swipeRight());
        onView(withId(R.id.viewpager_monthcontent)).perform(swipeRight());
        onView(withId(R.id.viewpager_monthcontent)).perform(swipeRight());

    }


}

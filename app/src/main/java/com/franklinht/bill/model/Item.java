package com.franklinht.bill.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by franklin on 01/09/15.
 */
public class Item {

    @SerializedName("post_date")
    private String postDate;
    @SerializedName("amount")
    private String amount;
    @SerializedName("title")
    private String title;
    @SerializedName("index")
    private String index;
    @SerializedName("charges")
    private String charges;
    @SerializedName("href")
    private String href;

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}

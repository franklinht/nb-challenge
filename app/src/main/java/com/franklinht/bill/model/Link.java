package com.franklinht.bill.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by franklin on 01/09/15.
 */
public class Link {

    @SerializedName("boleto_email")
    private BoletoEmail boletoEmail;
    @SerializedName("barcode")
    private Barcode barcode;
    @SerializedName("self")
    private Self self;

    public BoletoEmail getBoletoEmail() {
        return boletoEmail;
    }

    public void setBoletoEmail(BoletoEmail boletoEmail) {
        this.boletoEmail = boletoEmail;
    }

    public Barcode getBarcode() {
        return barcode;
    }

    public void setBarcode(Barcode barcode) {
        this.barcode = barcode;
    }

    public Self getSelf() {
        return self;
    }

    public void setSelf(Self self) {
        this.self = self;
    }

    public class LinkGenerico {
        @SerializedName("href")
        private String href;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public class BoletoEmail extends LinkGenerico{}
    public class Barcode extends LinkGenerico{}
    public class Self extends LinkGenerico{}

}

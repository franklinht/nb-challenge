package com.franklinht.bill.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by franklin on 02/09/15.
 */
public class Historico {

    @SerializedName("bill")
    private Bill bills;

    public Bill getBills() {
        return bills;
    }

    public void setBills(Bill bills) {
        this.bills = bills;
    }
}

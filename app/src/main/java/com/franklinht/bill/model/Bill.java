package com.franklinht.bill.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by franklin on 01/09/15.
 */
public class Bill {

    @SerializedName("state")
    private String state;
    @SerializedName("id")
    private String id;
    @SerializedName("barcode")
    private String barcode;
    @SerializedName("linha_digitavel")
    private String linhaDigitavel;

    @SerializedName("_links")
    private Link links;
    @SerializedName("line_items")
    private Item[] items;

    @SerializedName("summary")
    private Summary summary;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getLinhaDigitavel() {
        return linhaDigitavel;
    }

    public void setLinhaDigitavel(String linhaDigitavel) {
        this.linhaDigitavel = linhaDigitavel;
    }

    public Link getLinks() {
        return links;
    }

    public void setLinks(Link links) {
        this.links = links;
    }

    public Item[] getItems() {
        return items;
    }

    public void setItems(Item[] items) {
        this.items = items;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }
}

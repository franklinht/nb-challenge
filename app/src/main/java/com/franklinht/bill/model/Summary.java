package com.franklinht.bill.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by franklin on 01/09/15.
 */
public class Summary {

    @SerializedName("due_date")
    private String dueDate;
    @SerializedName("close_date")
    private String closeDate;
    @SerializedName("past_balance")
    private String pastBalance;
    @SerializedName("total_balance")
    private String totalBalance;
    @SerializedName("interest")
    private String interest;
    @SerializedName("total_cumulative")
    private String totalCumulative;
    @SerializedName("paid")
    private String paid;
    @SerializedName("minimum_payment")
    private String minimumPayment;
    @SerializedName("open_date")
    private String openDate;

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public String getPastBalance() {
        return pastBalance;
    }

    public void setPastBalance(String pastBalance) {
        this.pastBalance = pastBalance;
    }

    public String getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(String totalBalance) {
        this.totalBalance = totalBalance;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getTotalCumulative() {
        return totalCumulative;
    }

    public void setTotalCumulative(String totalCumulative) {
        this.totalCumulative = totalCumulative;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getMinimumPayment() {
        return minimumPayment;
    }

    public void setMinimumPayment(String minimumPayment) {
        this.minimumPayment = minimumPayment;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }
}

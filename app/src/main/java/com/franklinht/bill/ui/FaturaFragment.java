package com.franklinht.bill.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.franklinht.bill.R;
import com.franklinht.bill.adapters.ItemFaturaAdapter;
import com.franklinht.bill.model.Historico;
import com.franklinht.bill.model.Item;
import com.franklinht.bill.utils.Constants;
import com.franklinht.bill.utils.Utils;
import com.franklinht.bill.utils.singleton.UserSession;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by franklin on 01/09/15.
 */

@EFragment
public class FaturaFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;

    @ViewById(R.id.listview_fatura)
    ListView lvFatura;

    public static FaturaFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        FaturaFragment fragment = new FaturaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fatura, container, false);
        Log.i("debug", String.valueOf(getArguments().getInt(ARG_PAGE)));
        int paginaAtual = getArguments().getInt(ARG_PAGE);
        Historico faturaMes = ((Historico)UserSession.getInstance().getHistoricoUserBill().toArray()[paginaAtual - 1]);

        //Format valor da fatura
        TextView valorFatura = (TextView) view.findViewById(R.id.textview_valor_pagamento);
        valorFatura.setText(Utils.formatNumber(faturaMes.getBills().getSummary().getTotalBalance()));

        //Set de valor pago - para faturas pagas
        String valorPago = faturaMes.getBills().getSummary().getPaid();
        if(valorPago!=null ){
            ((TextView) view.findViewById(R.id.textview_valor_pago)).setText(String.format(
                    getResources().getString(R.string.mask_valor),
                    Utils.formatNumber(valorPago)));
        }

        int color = view.getResources().getColor(R.color.fatura_aberta);
        int layoutBoletoVisibility = View.GONE;
        int layoutPagamentoVisibility = View.GONE;
        int layoutResumoVisibility = View.GONE;

        configuraFatura(getActivity(), view, Arrays.asList(faturaMes.getBills().getItems()));

        if(faturaMes.getBills().getState().equalsIgnoreCase(Constants.FATURA_ABERTA)){
            color = view.getResources().getColor(R.color.fatura_aberta);
            layoutBoletoVisibility = View.VISIBLE;
            layoutPagamentoVisibility = View.GONE;
            layoutResumoVisibility = View.GONE;
        }else if(faturaMes.getBills().getState().equalsIgnoreCase(Constants.FATURA_FECHADA)){
            color = view.getResources().getColor(R.color.fatura_fechada);
            layoutBoletoVisibility = View.VISIBLE;
            layoutPagamentoVisibility = View.GONE;
            layoutResumoVisibility = View.VISIBLE;
        }else if(faturaMes.getBills().getState().equalsIgnoreCase(Constants.FATURA_PAGA)){
            color = view.getResources().getColor(R.color.fatura_paga);
            layoutBoletoVisibility = View.GONE;
            layoutPagamentoVisibility = View.VISIBLE;
            layoutResumoVisibility = View.GONE;
        }else if(faturaMes.getBills().getState().equalsIgnoreCase(Constants.FATURA_PROXIMA)){
            color = view.getResources().getColor(R.color.fatura_proxima);
            layoutBoletoVisibility = View.GONE;
            layoutPagamentoVisibility = View.GONE;
            layoutResumoVisibility = View.GONE;
        }else{
            //DO SOMETHING
        }

        (view.findViewById(R.id.relativelayout_statusfatura)).setBackgroundColor(color);

        Calendar dueDate = Utils.convertStringToCalendar(faturaMes.getBills().getSummary().getDueDate(), Locale.ENGLISH);
        String dataVenciamento = String.format("%s %s", dueDate.get(Calendar.DAY_OF_MONTH), Utils.getMonthName3FromInt(dueDate.get(Calendar.MONTH)));
        ((TextView)view.findViewById(R.id.textview_vencimento)).setText(String.format(view.getResources().getString(R.string.mask_vencimento), dataVenciamento));

        //set em literais de periodo
        Calendar dataAbertura = Utils.convertStringToCalendar(faturaMes.getBills().getSummary().getOpenDate(), Locale.ENGLISH);
        Calendar dataFechamento = Utils.convertStringToCalendar(faturaMes.getBills().getSummary().getCloseDate(), Locale.ENGLISH);
        String periodo = String.format(getResources().getString(R.string.periodo_de) + " %s %s " + getResources().getString(R.string.periodo_ate) + " %s %s",
                dataAbertura.get(Calendar.DAY_OF_MONTH),
                Utils.getMonthName3FromInt(dataAbertura.get(Calendar.MONTH)),
                dataFechamento.get(Calendar.DAY_OF_MONTH),
                Utils.getMonthName3FromInt(dataFechamento.get(Calendar.MONTH)));
        ((TextView) view.findViewById(R.id.textview_periodo)).setText(periodo);

        //Visibilidade nas views
        view.findViewById(R.id.include_resumo).setVisibility(layoutResumoVisibility);
        view.findViewById(R.id.include_pagamento).setVisibility(layoutPagamentoVisibility);
        view.findViewById(R.id.include_geraboleto).setVisibility(layoutBoletoVisibility);

        return view;
    }

    public void configuraFatura(Context c, View v, List<Item> items){
        ListView lvFatura = (ListView) v.findViewById(R.id.listview_fatura);

        ItemFaturaAdapter faturaAdapter = new ItemFaturaAdapter(c, R.layout.item_detalhefatura, null, null, null, items);
        lvFatura.setAdapter(faturaAdapter);
    }

}

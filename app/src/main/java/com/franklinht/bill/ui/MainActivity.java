package com.franklinht.bill.ui;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.franklinht.bill.R;
import com.franklinht.bill.adapters.FragmentAdapter;
import com.franklinht.bill.model.Historico;
import com.franklinht.bill.service.FaturaService;
import com.franklinht.bill.utils.singleton.UserSession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

/**
 * Created by franklin on 01/09/15.
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends FragmentActivity {

    @ViewById(R.id.viewpager_monthcontent)
    ViewPager viewPager;
    @ViewById(R.id.tablayout_month)
    TabLayout tabLayout;

    /**
     * Servico para obter o historico de faturas
     */
    @AfterViews
    public void getUserBill(){
        try {
            Type collectionType = new TypeToken<Collection<Historico>>(){}.getType();
            List<Historico> enums = new Gson().fromJson(FaturaService.getFatura(), collectionType);

            UserSession.getInstance().setHistoricoUserBill(enums);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @AfterViews
    public void initializeFragments(){
        viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager(), getApplicationContext()));
        tabLayout.setupWithViewPager(viewPager);
    }



}

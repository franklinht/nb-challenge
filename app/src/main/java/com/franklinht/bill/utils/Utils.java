package com.franklinht.bill.utils;

import java.text.DateFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by franklin on 02/09/15.
 */
public class Utils {

    /**
     * Converte uma String em calendar.
     * @param dateString data em formato {@link String}
     * @param locale {@link Locale}
     * @return um Calendar
     */
    public static Calendar convertStringToCalendar(String dateString, Locale locale){
        Calendar cal = Calendar.getInstance();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",locale );
            cal.setTime(sdf.parse(dateString));
        }catch (ParseException pe){
            //TODO: tratar
            pe.printStackTrace();
        }
        return cal;
    }

    /**
     * Este metodo faz a transformacao monetaria no padrao brasileiro.
     * ie. 12345678 -> R$ 123.456,78
     * {@link Locale}
     * @param numbertToFormat formato de qualquer tipo
     * @return
     */
    public static String formatNumber(String numbertToFormat){
        String newNumber = numbertToFormat.replaceAll("\\.", "").replaceAll("\\,", "");
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        formatter.setMinimumFractionDigits(2);
        formatter.setMaximumFractionDigits(2);
        return formatter.format(Double.parseDouble(newNumber)/100);
    }

    /**
     * Faz a transformação de um numero de 1-12 para mes.
     * <b>ie.</> 3 -> Março
     * @param monthInt numero que indica um mês do ano
     * @return o mês do ano em {@link String}
     */
    public static String getMonthNameFromInt(int monthInt){
        String monthName = "";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (monthInt >= 0 && monthInt <= 11 ) {
            monthName = months[monthInt];
        }

        return monthName;
    }

    /**
     *  Faz a transformação de um numero de 1-12 para mes, com crop dos três primeiros digitos.
     *  Caso não seja desejado o crop, utilize <i>getMonthNameFromInt()</>
     * @param monthInt umero que indica um mês do ano
     * @return o mês do ano em {@link String}
     */
    public static String getMonthName3FromInt(int monthInt){
        return getMonthNameFromInt(monthInt).substring(0,3).toUpperCase();
    }
}

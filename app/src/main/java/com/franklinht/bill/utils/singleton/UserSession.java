package com.franklinht.bill.utils.singleton;

import com.franklinht.bill.model.Historico;

import java.util.Collection;

/**
 * Created by franklin on 02/09/15.
 */
public class UserSession {

    private static UserSession uInstance;
    private static Collection<Historico> historicoUserBill;

    public static UserSession getInstance() {
        if (uInstance == null) {
            uInstance = new UserSession();
        }
        return uInstance;
    }

    public Collection<Historico> getHistoricoUserBill() {
        return historicoUserBill;
    }

    public void setHistoricoUserBill(Collection<Historico> historicoUserBill) {
        UserSession.historicoUserBill = historicoUserBill;
    }
}

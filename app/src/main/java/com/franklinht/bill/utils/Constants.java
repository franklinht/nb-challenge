package com.franklinht.bill.utils;

/**
 * Created by franklin on 02/09/15.
 */
public class Constants {
    public static String FATURA_PAGA    = "overdue";
    public static String FATURA_ABERTA  = "open";
    public static String FATURA_FECHADA = "closed";
    public static String FATURA_PROXIMA = "future";


}

package com.franklinht.bill.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.franklinht.bill.R;
import com.franklinht.bill.ui.FaturaFragment;

/**
 * Created by franklin on 01/09/15.
 */
public class FragmentAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {

    private Context context;

    public FragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return context.getResources().getStringArray(R.array.months).length;
    }

    @Override
    public Fragment getItem(int position) {
        return FaturaFragment.newInstance(position + 1);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getResources().getStringArray(R.array.months)[position];
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

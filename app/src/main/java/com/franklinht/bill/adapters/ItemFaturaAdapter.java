package com.franklinht.bill.adapters;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.franklinht.bill.R;
import com.franklinht.bill.model.Item;
import com.franklinht.bill.utils.Utils;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by franklin on 01/09/15.
 */
public class ItemFaturaAdapter extends SimpleCursorAdapter{

    private Context mContext;
    private List<Item> items;

    public ItemFaturaAdapter(Context context, int layout, Cursor cursor, String[] from, int[] to, List<Item> items ) {
        super(context, layout, cursor, null, null);
        this.mContext = context;
        this.items = items;

    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int arg0) {
        return 0;
    }

    public View getView(int position, View v, ViewGroup parent) {
        LayoutInflater inflate = ((Activity) mContext).getLayoutInflater();
        View view = (View) inflate.inflate(R.layout.item_detalhefatura, null);

        TextView tvDia = (TextView) view.findViewById(R.id.textview_dia);
        TextView tvMes = (TextView) view.findViewById(R.id.textview_mes);
        TextView tvDescricao = (TextView) view.findViewById(R.id.textview_descricao);
        TextView tvValor = (TextView) view.findViewById(R.id.textview_valor);

        Item itemFatura = items.get(position);
        try {
            Calendar cal = Utils.convertStringToCalendar(itemFatura.getPostDate(), Locale.ENGLISH);

            int num = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            String month = Utils.getMonthName3FromInt(num);

            tvDia.setText(String.valueOf(day));
            tvMes.setText(String.valueOf(month));
            tvDescricao.setText(itemFatura.getTitle());
            tvValor.setText(Utils.formatNumber(itemFatura.getAmount()));
        }catch(Exception e){

        }
        return view;
    }
}

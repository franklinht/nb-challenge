package com.franklinht.bill.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by franklin on 04/09/15.
 */
public class RedeReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if(intent.getExtras()!=null) {
            NetworkInfo networkInfo = (NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
            if(networkInfo!=null && networkInfo.getState()==NetworkInfo.State.CONNECTED) {
                Log.d("NB", "COM REDE");
            }
        }
        if(intent.getExtras().getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY,Boolean.FALSE)) {
            Log.d("NB", "SEM REDE");
        }
    }
}
